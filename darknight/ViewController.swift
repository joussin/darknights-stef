//
//  ViewController.swift
//  darknight
//
//  Created by stephane joussin on 17/08/2019.
//  Copyright © 2019 joussin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
 
 
    var gameService : GameService!
 
    @IBOutlet weak var eventLabel: UILabel!

    @IBOutlet weak var leftMoveButton: UIButton!
    @IBOutlet weak var rightMoveButton: UIButton!
    @IBOutlet weak var forwardMoveButton: UIButton!
    @IBOutlet weak var backwardMoveButton: UIButton!

    
    @IBAction func click(_ sender: UIButton) {
        self.gameService.user.move(sender.tag)
        self.eventLabel.text = " x : \(self.gameService.user.currentPosition.x) y: \(self.gameService.user.currentPosition.y) "
        self.gameService.checkForFixedPoint()
        
     
        // for test
        // it's same instance due of singleton
        let user = self.getContainer().resolve(UserService.self)!
         print(user.currentPosition.x)
        print(self.gameService.user.currentPosition.x)
    }
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        self.gameService = self.getContainer().resolve(GameService.self)!
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

