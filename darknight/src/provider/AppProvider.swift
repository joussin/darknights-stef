//
//  AppProvider.swift
//  darknight
//
//  Created by stephane joussin on 18/08/2019.
//  Copyright © 2019 joussin. All rights reserved.
//

import Foundation
import UIKit
import Swinject


extension UIViewController {
    // DOC HERE : https://github.com/Swinject/Swinject
    private func provider() -> Void {
        var app = UIApplication.shared.delegate as! AppDelegate
        // we create container once
        if( app.container == nil ) {
            // from AppDelegate
            app.container = Container()
            /* ----------------------------
             * ----------------------------
             * ADD YOUR SERVICES HERE
             * ----------------------------
             * ----------------------------
             */
            // user service
            app.container.register(UserService.self) { _ in UserService()}
                .inObjectScope(.container) // <- this line is to create a singleton of the service in the container, wherever your are the resolved instance is the same.
                    // remove this to have multiple instance 
                    // doc scope : https://github.com/Swinject/Swinject/blob/master/Documentation/ObjectScopes.md
            //map service
            app.container.register(MapService.self) { _ in MapService()}
                .inObjectScope(.container)
            // game service
            app.container.register(GameService.self) { _ in GameService(
                app.container.resolve(UserService.self)!,
                map : app.container.resolve(MapService.self)! )}
                .inObjectScope(.container)
        }
    }
    
    public func getContainer() -> Container {
        self.provider()
        var app = UIApplication.shared.delegate as! AppDelegate
        return app.container
    }
}
