//
//  GameService.swift
//  darknight
//
//  Created by stephane joussin on 18/08/2019.
//  Copyright © 2019 joussin. All rights reserved.
//

import Foundation
import UIKit


class GameService {
    
    var user : UserService!
    var map : MapService!
    
    init( _ user : UserService,  map : MapService ) {
        // initialise the stored properties here.
        self.user = user
        self.map = map
    }
    
    func checkForFixedPoint () {
        for fixedPoint in self.map.fixedPoints {
            if (fixedPoint["x"] as! Int) == Int(self.user.currentPosition.x) &&
                (fixedPoint["y"] as! Int) == Int(self.user.currentPosition.y) {
                //dispatch event : see https://github.com/gongzhang/swift-event-dispatch
                let event = fixedPoint["event"]
                print("dispatch event : \(event)")
            }
        }
    }
    
}
