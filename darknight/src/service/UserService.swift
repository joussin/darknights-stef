//
//  UserService.swift
//  darknight
//
//  Created by stephane joussin on 18/08/2019.
//  Copyright © 2019 joussin. All rights reserved.
//

import Foundation
import UIKit



class UserService  {

    var currentPosition: CGPoint = CGPoint(x: 0, y: 0)
    
    func move(_ movement : Int ){
        switch movement {
        case 0:
            self.currentPosition.y = self.currentPosition.y - 1
        case 1:
            self.currentPosition.x += 1
        case 2:
            self.currentPosition.y += 1
        case 3:
            self.currentPosition.x = self.currentPosition.x - 1
        default:
            break
        }
    }
}
